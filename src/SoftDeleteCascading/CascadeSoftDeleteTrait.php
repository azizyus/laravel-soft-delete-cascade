<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 26.12.2018
 * Time: 14:55
 */

namespace App\SoftDeleteCascading;


trait CascadeSoftDeleteTrait
{

    protected function cascades(){


        return [];

    }


    public function delete()
    {
        $this->deleteCascades();
        parent::delete();


    }
    public function deleteCascades()
    {

        foreach ($this->cascades() as $item)
        {


            $result = $this->$item->all();
            foreach ($result as $item)
            {

                $item->delete();
            }


        }

    }

}